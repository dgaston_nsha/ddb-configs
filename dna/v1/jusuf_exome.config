# Template for a workflow using BWA MEM plus post-processing with Picard and the GATK.
# If no version number of an algorithm or resource is specified then tool-level configuration
# will be pulled from the production directory as indicated in the
# information section.

# If not specifying specific version numbers for software and resources above
# Will look in the production directory for individual tool-level configuration files
# production can be set to false or not included. Version number should refer to the
# version number of this workflow. The same is true for development option.
# production and development are named directories in the tool workflow folder
# for configurations.

# Algorithm Selection occurs in the Python script

[settings]
run_name: actinic
project: actinic
genome_version: GRCh37.75
insert_size: 250
coverage_threshold: 10
coverage_threshold2: 30
coverage_loci_threshold: 0.001
bad_mate_threshold: 0.10
quality_loci_threshold: 0.50
min_alt_af: 0.01
mq0_threshold: 50
var_qual_threshold: 10
map_qual_threshold: 10

[sambamba]
bin: sambamba
num_cores: 24
max_mem:112

[vcfanno]
bin: /mnt/shared-data/Software/vcfanno_0.0.11_linux_amd64/vcfanno
num_cores: 24
max_mem: 112
conf: /mnt/shared-data/Resources/vcfanno.conf
lua: /mnt/shared-data/Resources/vcfanno.lua

[bwa]
bin: bwa
num_cores: 24
max_mem: 112

[ensemble]
bin: bcbio-variation-recall
num_cores: 24
max_mem: 112

# Because of docker use the -Xmx2g is currently specified here will fix this later
[gatk]
bin: java -Xmx24g -jar /mnt/shared-data/anaconda2/envs/ddb/bin/GenomeAnalysisTK.jar
# bin: docker run -v /mnt/shared-data:/mnt/shared-data -v $(pwd):/tmp gatk java -Xmx2g -jar /usr/bin/GenomeAnalysisTK.jar
max_mem: 112
num_cores: 24

# Because of docker use the -Xmx2g is currently specified here will fix this later
[gatk-filter]
bin: java -Xmx24g -jar /mnt/shared-data/anaconda2/envs/ddb/bin/GenomeAnalysisTK.jar
# bin: docker run -v /mnt/shared-data:/mnt/shared-data -v $(pwd):/tmp gatk java -Xmx2g -jar /usr/bin/GenomeAnalysisTK.jar
max_mem: 112
num_cores: 24

# Because of docker use the -Xmx2g is currently specified here will fix this later
[gatk-annotate]
bin: java -Xmx24g -jar /mnt/shared-data/anaconda2/envs/ddb/bin/GenomeAnalysisTK.jar
# bin: docker run -v /mnt/shared-data:/mnt/shared-data -v $(pwd):/tmp gatk java -Xmx2g -jar /usr/bin/GenomeAnalysisTK.jar
max_mem: 112
num_cores: 24

# Because of docker use the -Xmx2g is currently specified here will fix this later
[gatk-realign]
bin: java -Xmx24g -jar /mnt/shared-data/anaconda2/envs/ddb/bin/GenomeAnalysisTK.jar
# bin: docker run -v /mnt/shared-data:/mnt/shared-data -v $(pwd):/tmp gatk java -Xmx2g -jar /usr/bin/GenomeAnalysisTK.jar
max_mem: 112
num_cores: 24

# Because of docker use the -Xmx2g is currently specified here will fix this later
[gatk-recal]
bin: java -Xmx24g -jar /mnt/shared-data/anaconda2/envs/ddb/bin/GenomeAnalysisTK.jar
# bin: docker run -v /mnt/shared-data:/mnt/shared-data -v $(pwd):/tmp gatk java -Xmx2g -jar /usr/bin/GenomeAnalysisTK.jar
max_mem: 112
num_cores: 24

# Because of docker use the -Xmx2g is currently specified here will fix this later
[gatk-haplotypecaller]
bin: java -Xmx48g -jar /mnt/shared-data/anaconda2/envs/ddb/bin/GenomeAnalysisTK.jar
# bin: docker run -v /mnt/shared-data:/mnt/shared-data -v $(pwd):/tmp gatk java -Xmx2g -jar /usr/bin/GenomeAnalysisTK.jar
max_mem: 112
num_cores: 24

# Because of docker use the -Xmx2g is currently specified here will fix this later
[gatk-jointgenotyper]
bin: java -Xmx48g -jar /mnt/shared-data/anaconda2/envs/ddb/bin/GenomeAnalysisTK.jar
# bin: docker run -v /mnt/shared-data:/mnt/shared-data -v $(pwd):/tmp gatk java -Xmx2g -jar /usr/bin/GenomeAnalysisTK.jar
max_mem: 112
num_cores: 24

[gatk3.5]
bin: java -Xmx20g -jar /mnt/shared-data/Software/GenomeAnalysis3.5TK.jar
# bin: docker run -v /mnt/shared-data:/mnt/shared-data -v $(pwd):/tmp gatk3.5 java -Xmx2g -jar /usr/bin/GenomeAnalysis3.5TK.jar
max_mem: 20
num_cores: 24


[picard]
bin: java -Xmx24g -jar /mnt/shared-data/Software/picard-tools-1.141/picard.jar
max_mem: 24
num_cores: 24

[picard-dedup]
bin: java -Xmx24g -jar /mnt/shared-data/Software/picard-tools-1.141/picard.jar
max_mem: 24
num_cores: 24

[picard-add]
bin: java -Xmx24g -jar /mnt/shared-data/Software/picard-tools-1.141/picard.jar
max_mem: 24
num_cores: 24

[gemini]
bin: gemini
num_cores: 24
max_mem: 112

[snpeff]
bin: snpEff
reference: GRCh37.75
num_cores: 24
max_mem: 48

[bcftools]
bin: bcftools

[vcftools_sort]
bin: vcf-sort

[vcftools_subset]
bin: vcf-subset

[vcftools_isec]
bin: vcf-isec

[vcftools_merge]
bin: vcf-merge

[samtools]
bin: samtools

[fastqc]
bin: fastqc
cores: 1
max_mem: 2

[vt]
bin: vt

# Global resources
[resources]
regions: /mnt/shared-data/Resources/Annotations/ccds.bed
transcripts: /mnt/shared-data/ddb-configs/annotation/cancer_clinical_prefered_transcripts.list
reference: /mnt/shared-data/Resources/ReferenceData/b37/human_g1k_v37.fasta
dict: /mnt/shared-data/Resources/ReferenceData/b37/human_g1k_v37.dict
indel1: /mnt/shared-data/Resources/ReferenceData/b37/Mills_and_1000G_gold_standard.indels.b37.vcf
indel2: /mnt/shared-data/Resources/ReferenceData/b37/1000G_phase1.indels.b37.vcf
dbsnp: /mnt/shared-data/Resources/ReferenceData/b37/dbsnp_138.b37.vcf
